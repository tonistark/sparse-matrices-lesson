import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        SparseMatrix<Integer> matrix = new SparseMatrix<>(10, 10, 0);
        System.out.println("Initial matrix:");
        matrix.printMatrix();
        matrix.insert(0, 4, 3);
        matrix.insert(0, 8, 7);
        matrix.insert(0, 5, 4);
        matrix.insert(0, 9, 9);
        matrix.insert(0, 2, 1);
        matrix.insert(4, 2, 9);
        matrix.insert(8, 5, 2);
        matrix.insert(2, 5, 2);

        System.out.println("\nCurrent size: " + matrix.size());
        matrix.displayAllNodesWithinBounds();
        matrix.printMatrix();

        matrix.setRowSize(5);
        matrix.setColSize(5);
        System.out.println("\nAfter resize smaller, size: " + matrix.size());
        matrix.displayAllNodesWithinBounds();
        matrix.printMatrix();

        matrix.remove(0, 4);
        matrix.remove(4, 2);
        System.out.println("\nAfter two removals, size: " + matrix.size());
        matrix.printMatrix();

        matrix.setRowSize(15);
        matrix.setColSize(15);
        System.out.println("\nAfter resize larger");
        matrix.printMatrix();
        matrix.clearMatrix();
        System.out.println("\nAfter clear");
        matrix.printMatrix();
    }

}















/**
 * This class defines a very basic sparse matrix.
 *
 * @author Toni N. Tran
 *
 * @param <T>
 *            The type of data that will be stored in the sparse matrix.
 */
final class SparseMatrix<T> {

    /** The character that will take the place of an empty slot. */
    private T DEFAULT_VALUE = null;
    /** Instance max no. of rows and cols. */
    private int maxRows, maxCols;
    /** The array of ArrayList of MatNodes that make up our matrix. */
    private ArrayList<MatNode>[] rowArray;



    /**
     * Default ctor.
     *
     * @param rowSize
     *            The number of rows in the matrix.
     * @param colSize
     *            The number of columns in the matrix.
     * @param defaultChar
     *            The default value that will represent an empty spot.
     */
    public SparseMatrix(int rowSize, int colSize, T defaultChar) {
        /* Set row sizes and col sizes first. They will check for bad values. */
        setRowSize(rowSize);
        setColSize(colSize);
        setDefault(defaultChar);
        /*
         * If all is well, intantiate the array and fill each index with a new
         * ArrayList.
         */
        rowArray = new ArrayList[rowSize];
        for (int i = 0; i < rowArray.length; i++) {
            rowArray[i] = new ArrayList<>();
        }
        /* Initially set every value to the default value. */
        for (int r = 0; r < maxRows; r++) {
            for (int c = 0; c < maxCols; c++) {
                insert(r, c, this.DEFAULT_VALUE);
            }
        }
    }



    /**
     * Sets the default value for an empty node in the matrix.
     *
     * @param defaultChar
     *            The empty object, such as the integer 0, the character '0', or
     *            some other object.
     */
    public void setDefault(T defaultChar) {
        DEFAULT_VALUE = defaultChar;
    }



    /**
     * Mutator for the sparse matrix row size. The size can be changed at any
     * time. Any existing nodes out in far space will remain, but become
     * inaccessible and will not be printed.
     *
     * @param size
     *            The number of rows desired in the matrix.
     * @throws IllegalArgumentException
     *             if the size is negative.
     */
    public void setRowSize(int size) {
        if (size < 0) {
            throw new IllegalArgumentException("Invalid row argument: " + size);
        }
        /*
         * First time instantiation. Do not attempt any resizing or array
         * copying.
         */
        if (maxRows == 0) {
            maxRows = size;
            return;
        }
        /*
         * If we are setting the row to be larger than our current size,
         * increase its size.
         */
        if (size > maxRows) {
            ArrayList<MatNode>[] biggerArray = new ArrayList[size];
            System.arraycopy(rowArray, 0, biggerArray, 0, maxRows);
            for (int i = maxRows; i < size; i++) {
                biggerArray[i] = new ArrayList<>();
            }
            rowArray = biggerArray;
        }
        maxRows = size;
    }



    /**
     * Mutator for the sparse matrix column size. The size can be changed at any
     * time. Any existing nodes out in far space will remain, but become
     * inaccessible and will not be printed.
     *
     * @param size
     *            The number of columns desired in the matrix.
     * @throws IllegalArgumentException
     *             if the size is negative.
     */
    public void setColSize(int size) {
        if (size < 0) {
            throw new IllegalArgumentException("Invalid col argument: " + size);
        }
        maxCols = size;
    }



    /** @return The sparse matrix max row size. */
    public int getRowSize() {
        return maxRows;
    }



    /** @return The sparse matrix max column size. */
    public int getColSize() {
        return maxCols;
    }



    /**
     * Inserts an element into any row and column in the sparse matrix, so long
     * as it falls within the bounds.
     *
     * @param rowNum
     *            The row number (counting from 0).
     * @param colNum
     *            The col number (counting from 0).
     * @param data
     *            The data to wrap.
     * @return True if insertion went well, false if not.
     * @throws IllegalArgumentException
     *             if the passed in row or column numbers are out of bounds of
     *             the matrix.
     */
    public boolean insert(int rowNum, int colNum, T data) {
        /*
         * Test for bad data. If a row or col number is negative or out of
         * bounds of our max matrix size, return false.
         */
        if (rowNum >= maxRows || rowNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + rowNum);
        } else if (colNum >= maxCols || colNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + colNum);
        }
        /*
         * Return false if the client tries to set a slot to the default
         * character. That would be redundant and pointless, so don't even do
         * it.
         */
        if (data == DEFAULT_VALUE) {
            return false;
        }
        /* Search for a position within that current row to insert a new node. */
        ArrayList<MatNode> currentRow = rowArray[rowNum];
        MatNode m, newNode;
        for (int i = 0; i < currentRow.size(); i++) {
            m = currentRow.get(i);
            /*
             * If the node we are examining has a column number greater that our
             * prospective node, insert after it.
             */
            if (colNum < m.column) {
                newNode = new MatNode(colNum, data);
                currentRow.add(i, newNode);
                return true;
            }
        }
        /* We are inserting at the very end of the row. */
        newNode = new MatNode(colNum, data);
        currentRow.add(currentRow.size(), newNode);
        return true;
    }



    /**
     * Removes a node, setting it to the default value.
     *
     * @param rowNum
     *            The row of the node.
     * @param colNum
     *            The col of the node.
     * @return true if deletion was successful.
     * @throws IllegalArgumentException
     *             if the passed in row or column numbers are out of bounds of
     *             the matrix.
     */
    public boolean remove(int rowNum, int colNum) {
        /*
         * Test for bad data. If a row or col number is negative or out of
         * bounds of our max matrix size, return false.
         */
        if (rowNum >= maxRows || rowNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + rowNum);
        } else if (colNum >= maxCols || colNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + colNum);
        }
        /* Matrix is empty, nothing to remove. */
        if (this.isEmpty()) {
            return false;
        }
        /* Search for the node to remove within the row. */
        ArrayList<MatNode> currentRow = rowArray[rowNum];
        MatNode m;
        for (int i = 0; i < currentRow.size(); i++) {
            m = currentRow.get(i);
            if (m.column == colNum) {
                currentRow.remove(i);
                return true;
            }
        }
        return false;
    }



    /**
     * Retrieves a node's data at row, col. It returns the DEFAULT if there is
     * nothing there, or the data that is in the node if it is present.
     *
     * @param rowNum
     *            The row to inspect.
     * @param colNum
     *            The col to inspect.
     * @return The node's data if present, or the DEFAULT otherwise.
     * @throws IllegalArgumentException
     *             if the passed in row or column numbers are out of bounds of
     *             the matrix.
     */
    public T get(int rowNum, int colNum) {
        /*
         * Test for bad data. If a row or col number is negative or out of
         * bounds of our max matrix size, return false.
         */
        if (rowNum >= maxRows || rowNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + rowNum);
        } else if (colNum >= maxCols || colNum < 0) {
            throw new IllegalArgumentException("Invalid argument: " + colNum);
        }
        /* Find the node to retrieve the value of. */
        ArrayList<MatNode> currentRow = rowArray[rowNum];
        for (MatNode m : currentRow) {
            if (m.column == colNum) {
                return m.data;
            }
        }
        /*
         * Node was not found. We are searching an empty location, so return the
         * DEFAULT.
         */
        return DEFAULT_VALUE;
    }



    /**
     * Returns the number of elements currently stored in the sparse matrix.
     * This method is bounded by maxRow and maxCol.
     *
     * @return the number of elements currently stored in the sparse matrix.
     */
    public int size() {
        int size = 0;
        for (int i = 0; i < maxRows; i++) {
            for (MatNode m : rowArray[i]) {
                if (m.column < maxCols) {
                    size++;
                }
            }
        }
        return size;
    }



    /**
     * Returns true if the matrix has no elements in it.
     *
     * @return true if the matrix has no elements in it.
     */
    public boolean isEmpty() {
        return size() == 0;
    }



    /**
     * Clears the entire matrix.
     */
    public void clearMatrix() {
        for (ArrayList<MatNode> list : rowArray) {
            list.clear();
        }
    }



    /**
     * Prints every single node present in the sparse matrix, bound within its
     * maxRow and maxCol values.
     */
    public void displayAllNodesWithinBounds() {
        /* Empty matrix. */
        if (this.isEmpty()) {
            System.out.println("Matrix is empty!");
            return;
        }
        for (int i = 0; i < maxRows; i++) {
            System.out.print("[Row: " + i + "]: ");
            ArrayList<MatNode> currentRow = rowArray[i];
            MatNode m;
            for (int j = 0; j < currentRow.size(); j++) {
                m = currentRow.get(j);
                if (m.column < maxCols) {
                    System.out.print(m);
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }



    /**
     * Prints the matrix as if it were a regular 2D array. This method DEPENDS
     * UPON THE SET MAX ROW AND COL SIZE DONE FROM CONSTRUCTION.
     */
    public void printMatrix() {
        /* Using a for loop, call get on every position. */
        for (int r = 0; r < maxRows; r++) {
            for (int c = 0; c < maxCols; c++) {
                System.out.print(this.get(r, c));
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    /**
     * A MatNode holds data concerning the row number, column number, and stored
     * data to be held within the Sparse Matrix.
     *
     * @author Toni N. Tran
     */
    class MatNode {

        T data;
        int column;



        /**
         * A MatrixNode constructor.
         *
         * @param dataToStore
         *            The data to store within the node.
         * @param rowNumber
         *            The row number of the node.
         * @param columnNumber
         *            The column number of the node.
         */
        public MatNode(int columnNumber, T dataToStore) {
            data = dataToStore;
            column = columnNumber;
        }



        /** @return a string representation of the node. Useful for debugging. */
        @Override
        public String toString() {
            return "(c" + column + ", " + data + ")";
        }

    }
}
